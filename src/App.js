//import statements
import { useState } from 'react';
import './App.css';
import Footer from './Footer';
import Header from './Header';
import Main from './Main';


function App() {
  const [search, setSearch] = useState("");
  const onSearch = (e) => {
    setSearch(e.target.value)
  }

  return (
    <div className="App">
      <Header search={search} onSearch={onSearch} />
      <Main search={search} />
      <Footer />
    </div>
  );
}

export default App;
