//Qiuning Liang

import Filter from "../MainColumnComponents/Filter";
import PostListItem from "../MainColumnComponents/PostListItem"



const PostText = () => {
    return <div className="post-text">
        <textarea rows="3"></textarea>
        <button>Submit Post</button>
    </div>
}

const MainColumn = ({
    forum,
    category, topic,
    onSelectCategory,
    onSelectTopic,
    posts,
    onLike,
    onDelete }) => {

    return (
        <section id="main-column">
            <Filter categories={forum.categories}
                onSelectCategory={onSelectCategory}
                topics={category.topicList}
                onSelectTopic={onSelectTopic}
                category={category}
                topic={topic}
            />
            <PostListItem posts={posts} onLike={onLike} onDelete={onDelete} />
            <PostText></PostText>
        </section>

    );
}

export default MainColumn;
