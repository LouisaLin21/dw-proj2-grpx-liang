//Qiuning Liang
//import MainNavBar from "./Navigation/MainNavBar"

const Header = ({ search, onSearch }) => {
    return (
        <header id="header">
            <h2>Forum</h2>
            {/* <MainNavBar /> */}
            <div id="warpper">
                <input id="search" value={search} onChange={onSearch} placeholder="Search the entire forum"></input>
            </div>
        </header>
    );
}

export default Header;
