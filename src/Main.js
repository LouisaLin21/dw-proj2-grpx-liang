/*Done by Louisa Lin and Qiuning Liang*/

/*import statements */
import { useEffect, useState } from "react";
import useFetchData from "./Hooks/useFetchdata";
import LeftColumn from "./MainComponents/LeftColumn";
import MainColumn from "./MainComponents/MainColumn";
import SideColumn from "./MainComponents/SideColumn";

/*main component */
const Main = ({ search }) => {

    const [forum, setForum] = useFetchData('./forum.json')
    const [category, setCategory] = useState({ id: '' })
    const [topic, setTopic] = useState({})
    const [posts, setPosts] = useState([])


    const onSelectCategory = (id) => {
        const category = forum.categories.find((category) => category.id === Number(id))
        if (category) {
            setCategory(category)
            setTopic(category.topicList[0])
            setPosts(category.topicList[0].listPosts)
        } else {
            setCategory({})
            setTopic({})
            setPosts([])
        }
    }

    const onSelectTopic = (id) => {
        const topic = category.topicList.find(topic => topic.id == id)
        setTopic(topic)
        setPosts(topic.listPosts)
    }
//component for like
    const onLike = (topic_id, postId, likes) => {
        const copy = { ...forum };
        let arr = topic_id.split('_')
        const categoryId = Number(arr[0].slice(-1)[0]);
        const topicId = Number(arr[1].slice(-1)[0]);

        const c = forum.categories.find(c => c.id === categoryId)
        const t = c.topicList.find(t => t.id === topicId)
        const p = t.listPosts.find(p => p.id === postId)


        p['likes'] = Math.max(likes, 0)
        setForum(copy);

    }
    //Variable for deletion.
    const onDelete = (topic_id, postId) => {
        const copy = { ...forum };
        let arr = topic_id.split('_')
        const categoryId = Number(arr[0].slice(-1)[0]);
        const topicId = Number(arr[1].slice(-1)[0]);

        const c = forum.categories.find(c => c.id === categoryId)
        const t = c.topicList.find(t => t.id === topicId)
        t.listPosts = t.listPosts.filter(p => p.id !== postId)

        setForum(copy);
        setPosts(posts.filter(p => p.id !== postId))
    }

    useEffect(() => {
        let posts = []
        /*If Search bar is not null start searching using filter. */
        if (search !== '') {
            forum.categories.forEach(c => c.topicList.forEach(t => {
                posts = [...posts, ...(t.listPosts.filter(p => p.text.toLowerCase().includes(search.toLowerCase())))]
            }));
        }
        console.log(posts)
        setPosts(posts)
    }, [search])
//final product of this class
    return (
        <section id="main-content">
            <LeftColumn />
            <MainColumn
                forum={forum}
                category={category}
                onSelectCategory={onSelectCategory}
                topic={topic}
                onSelectTopic={onSelectTopic}
                posts={posts}
                onLike={onLike}
                onDelete={onDelete}
            />
            <SideColumn forum={forum} />
        </section>

    );
}

export default Main;
