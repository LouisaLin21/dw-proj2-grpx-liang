//Qiuning liang
import React from "react";

export default function useFetchData(url) {
    const [data, setData] = React.useState([]);
    async function fetchData(url) {
        try {
            let res = await fetch(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            let data = await res.json();
            setData(data)
        } catch (e) {
            alert('cant fetch from ' + url)
        }

    }

    React.useEffect(() => {
        fetchData(url)
    }, [])

    return [data, setData];
}
