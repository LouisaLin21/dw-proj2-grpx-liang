//Qiuning Liang
const Filter = ({ categories = [], category, topic, onSelectCategory, topics = [], onSelectTopic }) => {

    return <div className="filter">
    //  setting the category filed
        <div>
            <label htmlFor="category"><h1>Category</h1></label>
            <select id="category" value={category.id | ''} onChange={(e) => onSelectCategory(e.target.value)}>
                <option value="0" selected>----Select Category-----</option>
                {categories.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
            </select>
        </div>
        <div>
            <label htmlFor="topic" ><h1>Topic</h1></label>
            <select id="topic" value={topic.id | ''} onChange={(e) => onSelectTopic(e.target.value)}>
                {topics.map(topic => <option key={topic.id} value={topic.id}>{topic.topic_title}</option>)}
            </select>
        </div>
    </div>
}

export default Filter;
