//Qiuning Liang
const LeftColumn = () => {


    return (
        <section id="left-column">
            <div className="wrapper">

                <div className="buttons">
                    <h2>Admin</h2>
                    <button>Create Category</button>
                    <button>Create Topic</button>
                    <button>Close Topic</button>
                    <button>Delete Topic</button>
                </div>
            </div>
            {/* <TaskListItem /> */}
        </section>

    );
}

export default LeftColumn;
