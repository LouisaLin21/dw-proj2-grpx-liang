//Louisa Lin

import { useEffect, useState } from "react"
import useFetchData from "../Hooks/useFetchdata"

const RankTopic = ({ rows = [{ title: '123', number: 3, stat: 'ongoing' }, { title: '123', number: 3, stat: 'ongoing' }] }) => {

    return <section className="table-wrapper table-rank">
        <h2>Ranked Topics</h2>
        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Post No.</th>
                    <th>Stat</th>
                </tr>
            </thead>
            <tbody>
                {rows.map((row, index) => {
                    return <tr key={index}>
                        <td>{row.topic_title}</td>
                        <td>{row.nberPost}</td>
                        <td>{row.status}</td>
                    </tr>
                })}
            </tbody>
        </table>
    </section>
}


const RecentPosts = ({ rows = [] }) => {

    return <section className="table-wrapper table-recent">
        <h2>Recent Topics</h2>
        <table>
            <thead>
                <tr>
                    <th>Author</th>
                    <th>rank</th>
                    <th>When</th>
                </tr>
            </thead>
            <tbody>
                {rows.map((row, index) => {
                    return <tr key={index}>
                        <td>{row.author}</td>
                        <td>{row.rate}</td>
                        <td>{parseInt(row.days)} days ago</td>
                    </tr>
                })}
            </tbody>
        </table>
    </section>
}


const StatsPosts = ({ rows = [] }) => {

    return <section className="table-wrapper table-stats">
        <h2>Stats Topics</h2>
        <table>
            <tbody>
                {rows.map((row, index) => {
                    return <tr key={index}>
                        <td>{row.user_id}</td>
                        <td>{row.nberPosts}</td>
                    </tr>
                })}
            </tbody>
        </table>
    </section>
}

const SideColumn = ({ forum }) => {
    const [rankTopicRows, setRankTopicRows] = useState([])
    const [recentPostsRows, setRecentPostsRows] = useState([])
    const [statsPostRows, setStatsPostsRows] = useState([])
    const [users, setUsers] = useFetchData('/users.json')
    useEffect(() => {
        let topics = []
        let recentPosts = []
        if (forum.categories) {
            forum.categories.forEach(category =>
                topics = [...topics, ...category.topicList])

            topics.forEach(topic =>
                recentPosts = [...recentPosts, ...topic.listPosts])

        }

        recentPosts = recentPosts.map((post) => ({ ...post, days: (new Date().getTime() - new Date(post.date).getTime()) / (24 * 3600 * 1000) }))
            .sort((a, b) => a.days - b.days)

        setRankTopicRows(topics)
        setRecentPostsRows(recentPosts)

    }, [forum])

    useEffect(() => {
        if (users.users) {
            setStatsPostsRows(users.users)
        }
    }, [users])

    return (
        <section id="side-column">
            <RankTopic rows={rankTopicRows}></RankTopic>
            <RecentPosts rows={recentPostsRows}></RecentPosts>
            <StatsPosts rows={statsPostRows}></StatsPosts>
        </section>

    );
}
export default SideColumn;
